import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { generateCerti } from "../../actions/blockaction";
import { logoutUser } from "../../actions/authActions";


class Generate extends Component {
  constructor() {
    super();
    this.state = {
        documentid: "",
        universityid: "",
        studentid: "",
        studentname: "",
        documentname: "",
        sha: "",
        date: ""
    };
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };


  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
    console.log(e.target.value)
  };

  onSubmit = e => {
    e.preventDefault();
    let certiDetails={
        documentid:this.state.documentid,
        universityid:this.props.auth.user.id,
        studentid:this.state.studentid,
        studentname:this.state.studentname,
        docname:this.state.documentname
    }
    console.log(certiDetails)
    this.props.generateCerti(certiDetails);
  }

  render() {
    const { errors } = this.state;

    return (
        <div className="container">
        <div style={{ marginTop: "4rem" }} className="row">
        <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>
          <div className="col s8 offset-s2">
            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
              <h4>
                <b>Generate Certificate</b> 
              </h4>
              <p className="grey-text text-darken-1">
               Enter details to generate certificate
              </p>
            </div>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.documentid}
                  id="documentid"
                  type="text"
                />
                <label htmlFor="email">Document_ID</label>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.documentname}
                  id="documentname"
                  type="text"
                />
                <label htmlFor="password">Document_name</label>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.studentid}
                  id="studentid"
                  type="text"
                />
                <label htmlFor="password">Student_ID</label>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.studentname}
                  id="studentname"
                  type="text"
                />
                <label htmlFor="password">StudentName</label>
              </div>
              <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                <button
                  style={{
                    width: "150px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                    marginTop: "1rem"
                  }}
                  type="submit"
                  className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                >
                  Generate
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Generate.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  generateCerti: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  usertype:state.auth.user.type,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { generateCerti,logoutUser  }
)(Generate);