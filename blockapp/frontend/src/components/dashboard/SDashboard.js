import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import axios from 'axios'
import 'filepond/dist/filepond.min.css';

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      studentname: this.props.auth.user.name,
      card: [],
      veri:"",
      assetId:""
    }
  }
  async componentWillMount() {
    console.log("mounted")
    const response = await axios.post('/api/users/block/login/student/list', { studentname: this.state.studentname })
    const toDoItems = await response.data
    this.setState({ card: toDoItems })
    console.log("statecard", this.state.card)

  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  ongrantClick= (e,valu)=>{
    e.preventDefault();
  console.log("verider state befoer",this.state.veri)
  console.log("assset vale",valu )
   var id=this.state.veri
   var asset=this.state.card[0].assetId
    const response =  axios.post('api/users/block/login/student/accessgrant', { assetid: valu ,verifierid:id})
    const toDoItems = response.data
    //axios
    console.log(e)
  }
  
   handle=(e)=>{
    console.log("veriferid",e.target.value)
    this.setState({ veri : e.target.value})
  }
  
  render() {

    const { user } = this.props.auth;
    let data = this.state.card
    console.log("assetid",data)


    var jsx=data.map((element)=>{
      return(
        <div key={element.assetId} className ="row">
        <div className="col s12 m12">
          <div className="card blue-grey darken-1">
            <div className="card-content white-text">
              <span className="card-title">{element.documentname}</span>
              <p>SHA512 : {element.sha}</p>
              <p>Document ID : {element.assetId}</p>
              <p>Date of issue : {element.date}</p>
              <p>Student : {element.studentname}</p>
            </div>
            <div className="card-action"> 
            
            <form value={element.assetId} onSubmit={e=>this.ongrantClick(e,element.assetId)}>
            <input type="text" onChange={this.handle}/>
            <button type="submit" 
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              >grant</button>
            </form>
            </div>
          </div>
        </div>
      </div>
      
        )
    })

    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="col s12 center-align">
            <h4>
              <p className="flow-text grey-text text-darken-1">
              <b>Hey there Student,</b> {user.name.split(" ")[0]}
                <span style={{ fontFamily: "monospace" }}></span> app
              </p>
            </h4>
            <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>

            <h3>Certificates</h3>

            {jsx}


          </div>
        </div>

      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);