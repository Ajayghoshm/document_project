import React, { Component } from "react";

class Card extends Component{

    constructor(props) {
        super(props);
        
    }
    render(){
      let data=this.props.data
      console.log("card",data)
        return(
            <div className="row">
            <div className="col s12 m6">
              <div className="card blue-grey darken-1">
                <div className="card-content white-text">
                  <span className="card-title">{data}</span>
                  <p>I am a very simple card. I am good at containing small bits of information.
        I am convenient because I require little markup to use effectively.</p>
                </div>
                <div className="card-action">
                  <button>This is a link</button>
                  <a href="#">This is a link</a>
                </div>
              </div>
            </div>
          </div>
        )
    
    }
}
export default Card