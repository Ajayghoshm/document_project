import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import axios from 'axios'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';


class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      selectedFile: null, 
      loaded: 0,
      SHA: "nill" ,
    studentname: this.props.auth.user.name,
    card: [],
  valid:"not"}
  }

  async componentWillMount() {
    console.log("mounted")
    const response = await axios.post('/api/users/block/login/student/list', { studentname: this.state.studentname })
    const toDoItems = await response.data
    this.setState({ card: toDoItems })
    console.log("statecard", this.state.card)
  }
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };


  handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    })
  }

  handleChange=e=>{
    this.setState({ veri: e})
    console.log(e)
  }


  handleUpload = () => {
    const data = new FormData()
    data.append('file', this.state.selectedFile)
    data.append('filename', this.state.selectedFile.name)

    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };

    axios
      .post("/api/users/upload", data, config, {
        onUploadProgress: ProgressEvent => {
          this.setState({
            loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
          })
        },
      })
      .then(res => {
        console.log(res.statusText)
      })

  }

  ongrantClick= (e,valu)=>{
    e.preventDefault();
  console.log("verider state befoer")
  console.log("assset vale",valu )
  }

  render() {
    const { user } = this.props.auth;
    let data = this.state.card
    console.log('jsx', data)

    let updatefile=(fileItems)=>{
              this.setState({
                  files: fileItems.map(fileItem => fileItem.file)
                });
    }

    let sha_change=(value,elementsha)=>{
      this.setState({SHA:value})
      if(this.state.SHA==elementsha){
        this.setState({valid:""})
      }
      else{
        this.setState({valid:"not"})
      }
      
    }

    var jsx=data.map(function(element){
      return(
        <div key={element.assetId} className ="row">
        <div className="col s12 m12">
          <div className="card blue-grey darken-1">
            <div className="card-content white-text">
              <span className="card-title">{element.documentname}</span>
              <p>SHA512 : {element.sha}</p>
              <p>Document ID : {element.assetId}</p>
              <p>Date of issue : {element.date}</p>
              <p>Student : {element.studentname}</p>
            </div>
            <div className="card-action">
              
            <FilePond server={
              {
                process: {
                  url: 'api/users/block/login/verifier/verify',
                  onload: (res) => {
                    // select the right value in the response here and return
                    console.log(res)
                    var values = JSON.parse(res)
                    console.log(values.SHA)
                    sha_change(values.SHA,element.sha)
                  },
                  onerror: null
                }
              }} onupdatefiles={(fileItems) => {
                // Set current file objects to this.state
                updatefile(fileItems)
                console.log("files",fileItems)
              }}>
              </FilePond>
            </div>
          </div>
        </div>
      </div>
      
        )
    })


    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="col s12 center-align">
            <h4>
              <b>Hey there Verifier,</b> {user.name.split(" ")[0]}
              <p className="flow-text grey-text text-darken-1">
                You are logged into a Human Resource data verification using blockchain{" "}
                <span style={{ fontFamily: "monospace" }}></span> app
              </p>
            </h4>
            <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>
            {jsx}
            <div>
              <h4>Certificate is {this.state.valid} Valid</h4>
            </div>
            <div>
            <h6>SHA value= {this.state.SHA}  </h6>
            </div>
            
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);