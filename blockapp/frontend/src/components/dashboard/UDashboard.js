import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import axios from 'axios'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

class Dashboard extends Component {

  constructor() {
    super();
    this.state = { selectedFile: null, loaded: 0, SHA: "nill" }
    this.sha_change = this.sha_change.bind(this);
  }

  sha_change(value) {
    this.setState({ SHA: value })
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  GenerateClick =()=>{
    this.props.history.push("/generate");
  }


  handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    })
  }


  handleUpload = () => {
    const data = new FormData()
    data.append('file', this.state.selectedFile)
    data.append('filename', this.state.selectedFile.name)

    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };

    axios
      .post("/api/users/upload", data, config, {
        onUploadProgress: ProgressEvent => {
          this.setState({
            loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
          })
        },
      })
      .then(res => {
        console.log(res.statusText)
      })

  }

  render() {
    const { user } = this.props.auth;


    return (
      <div style={{ height: "35vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="col s12 center-align">
            <button
              style={{
                width: "150px",
                borderRadius: "2px",
                letterSpacing: "1.5px",
                marginTop: "1rem",
                marginDown: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>
            <h4>
              <b>{user.name.split(" ")[0]}</b> University
            </h4>
            <button
              style={{
                width: "150px",
                borderRadius: "2px",
                letterSpacing: "1.5px",
                marginTop: "1rem",
                marginDown: "1rem"
              }}
              onClick={this.GenerateClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Generate
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);