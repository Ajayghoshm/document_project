import axios from "axios";
import {
  GET_ERRORS,
  GENERATE_CERTIFICATE
} from "./types";
const FileDownload = require('js-file-download');




export const generateCerti = userData => dispatch => {
  console.log(userData)

  axios({
    method: 'post',
    url: "api/users/block/login/university/createdocument",
    data: {
      "docname": userData.docname,
      "documentid": userData.documentid,
      "studentid": userData.studentid,
      "studentname": userData.studentname,
      "universityid": userData.universityid,
    },responseType: 'blob'
  }).then(res => {
    const file = new Blob(
      [res.data], 
      {type: 'application/pdf'});
      const fileURL = URL.createObjectURL(file);
      window.open(fileURL);
  })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
}

export const certificate = decoded => {
  return {
    type: GENERATE_CERTIFICATE,
    payload: decoded
  };

};

