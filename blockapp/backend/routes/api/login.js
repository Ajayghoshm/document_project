var router = require('express').Router();
const MyNetwork = require('../../block/mynetwork');
const validateLoginInput = require("../../validation/login");
const User = require("../../models/User");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const bcrypt = require("bcryptjs");
var sha256File = require('sha256-file')
const PDFDocument = require('pdfkit');
const crypto = require('crypto');
const fs = require('fs')

var globalfile


// api/products
router.post('/student', function (req, res) {
    console.log("student")

    // Form validation
    console.log("login", req.body)

    const { errors, isValid } = validateLoginInput(req.body);

    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    // Find user by email
    User.findOne({ email }).then(user => {
        // Check if user exists
        if (!user) {
            return res.status(404).json({ emailnotfound: "Email not found" });
        }
        // Check password
        bcrypt.compare(password, user.password).then(isMatch => {
            if (isMatch) {
                // User matched
                // Create JWT Payload
                mynetwork = new MyNetwork()
                console.log(user.name)
                mynetwork.View(user.name)
                const payload = {
                    id: user.id,
                    name: user.name,
                    type: user.typeid
                };

                // Sign token
                jwt.sign(
                    payload,
                    keys.secretOrKey,
                    {
                        expiresIn: 31556926 // 1 year in seconds
                    },
                    (err, token) => {
                        res.json({
                            success: true,
                            token: "Bearer " + token
                        });
                    }
                );
            } else {
                return res
                    .status(400)
                    .json({ passwordincorrect: "Password incorrect" });
            }
        });
    });
});

transaction = (req, sha512) => {
    var transactionDetails = {
        documentid: req.body.documentid,
        universityid: req.body.universityid,
        studentid: req.body.studentid,
        studentname: req.body.studentname,
        documentname: req.body.docname,
        sha: sha512,
        date: new Date().toISOString()
    }
    mynetwork = new MyNetwork()
    mynetwork.createAssetTransaction(transactionDetails)

}

generatesha = (file) => {
    return new Promise(resolve => {
        var crypto = require('crypto');
        //creating hash object 
        var hash = crypto.createHash('sha512');
        //passing the data to be hashed
        data = hash.update(file);
        //Creating the hash in the required format
        hashvale = data.digest('hex');
        //Printing the output on the console
        console.log("hash : " + hashvale);
        resolve(hashvale)
    })
}
createFile = (req) => {
    return new Promise(resolve => {
        const doc = new PDFDocument;
        var Document = {
            documentid: req.body.documentid,
            universityid: req.body.universityid,
            studentid: req.body.studentid,
            studentname: req.body.studentname,
            documentname: req.body.docname,
            date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') 
        }
        writestream = fs.createWriteStream(Document.documentname + Document.documentid + '.pdf')
        let filename = `${__basedir}/${Document.documentname + Document.documentid}.pdf`
        doc.pipe(writestream);
        
        
        doc.fontSize(50)
            .text(Document.documentname + " Certificate ");
            doc.moveDown(2)
            doc.fontSize(25).text("Document ID : "+ Document.documentid )
            doc.fontSize(25).text("Student Name : "+ Document.studentname )
            doc.fontSize(25).text("Student ID : "+ Document.studentid)
            doc.fontSize(25).text("Issued Date : "+ Document.date)
            doc.fontSize(25).text("University Id : "+ Document.universityid)
            
            doc.end();
        writestream.on('finish', () => {
            console.log("file write completed")
            fs.readFile(filename, (err, data) => {
                if (err) {
                    console.log("error wile reading", err)
                    resolve("err")
                }
                else {
                    console.log("readfilestream", data)
                    resolve(data)
                }
            })
        })




    })

}

router.post("/university/createdocument", (req, res) => {

    let fileName;
    var options = {
        root: __basedir,
        dotfiles: 'allow',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    createFile(req).then((data) => {
        console.log("resolved", data)
        generatesha(data).then((sha) => {
            console.log("hashvalue", sha)
            transaction(req, sha)

            fileName = `${req.body.docname + req.body.documentid}.pdf`
            res.sendFile(fileName, options, function (err) {
                if (err) {
                    console.log("senderor", err)
                } else {
                    console.log('Sent:', fileName);
                }
            });
        })
    })

    /*var file = createFile(req);
    globalfile=file;
    var sha= generatesha(file)
    */




    /*fileloc=`${__basedir}/${req.body.docname + req.body.documentid}.pdf`
    console.log("file name",fileloc)
   res.sendFile(fileloc, function (err) {
        if (err) {
          console.log("senderor",err)
        } else {
          console.log('Sent:', file);
        }
      });
      console.log(file)*/


    /* res.writeHead(200, {
       'Content-Type': 'application/pdf',
       'Content-Disposition': 'attachment',
       'Content-Length': file.length
     });
     res.end(file)*/


    /*console.log(`${__basedir}/${Document.documentname + Document.documentid}.pdf`)
    sha256File(`${__basedir}/${Document.documentname + Document.documentid}.pdf`, function (error, sum) {
        if (error) return console.log("error:", error);
       // console.log("sum", sum)
        sha512 = crypto.createHash('sha512').update(`${__basedir}/${Document.documentname + Document.documentid}.pdf`).digest('hex');
        console.log("sha512", sha512)
          fileName= `${Document.documentname + Document.documentid}.pdf`
            console.log("filename",fileName)
         // console.log("transactionsha",sha512)

          var transactionDetails = {
            documentid: req.body.documentid,
            universityid: req.body.universityid,
            studentid: req.body.studentid,
            studentname: req.body.studentname,
            documentname: req.body.docname,
            sha: sha512,
            date: new Date().toISOString()
        }

       /* var data =fs.readFileSync('./public/modules/datacollectors/output.pdf');
        res.contentType("application/pdf");
        res.send(data)
        //console.log(transactionDetails)
       mynetwork = new MyNetwork()
     mynetwork.createAssetTransaction(transactionDetails)
        res.sendFile(fileName, options, function (err) {
            if (err) {
              console.log("senderor",err)
            } else {
              console.log('Sent:', fileName);
            }
          });
        
        // '345eec8796c03e90b9185e4ae3fc12c1e8ebafa540f7c7821fb5da7a54edc704'
        /*res.json({
            File_Name: `${Document.documentname + Document.documentid}.pdf`,
            SHA: sha256,
        })
    })*/
})

router.post('/student/accessgrant', function (req, res) {
    console.log("access")
    var accessTransaction = {
        verifierid: req.body.verifierid,
        asset: req.body.assetid
    }
    console.log(accessTransaction.asset + accessTransaction.verifierid)
    mynetwork = new MyNetwork()
    mynetwork.addVerifierToAcess(accessTransaction).then(() => {
        res.json({ added: "added" })
    });

})

router.post("/student/list", (req, res) => {
    console.log("list")
    mynetwork = new MyNetwork();
    mynetwork.View(req.body.studentname).then((documentresults) => {
        documentresults.forEach(element => {
            console.log("route", element.owner.$identifier)
            console.log(element.documentname)
            console.log(element.assetId)
            console.log(element.studentname)
            console.log(element.sha)
            console.log("oute", element.date)
            element.access.forEach(value => {
                console.log(value.$identifier)
            })
            res.send(documentresults)
        })

    })
})

router.post("/student/addorgin", (req, res) => {
    var transferDetails = {
        studentid: req.body.studentid,
        asset: req.body.asset
    }
    mynetwork = new MyNetwork()
    mynetwork.View("ajay").then((allResults) => {
        let results = allResults.documentresults
        mynetwork.transferOwnerTransaction(transferDetails)
        const doc = new PDFDocument;
        results.forEach((Element) => {
            doc.pipe(fs.createWriteStream(Element.documentname + Element.assetId + '.pdf'));
            doc.fontSize(25)
                .text(Element.documentname + Element.studentname + Element.assetId + Element.date, 100, 100);
            doc.end();
            sha256File(`${Element.documentname + Element.assetId}.pdf`, function (error, sum) {
                if (error) return console.log("error:", error);
                console.log("sum", sum)
                const sha256 = crypto.createHash('sha256').update(`${Element.documentname + Element.assetId}.pdf`).digest('hex');
                console.log("sha25", sha256)
                // '345eec8796c03e90b9185e4ae3fc12c1e8ebafa540f7c7821fb5da7a54edc704'
                res.json({
                    File_Name: `${Element.documentname + Element.assetId}.pdf`,
                    SHA: sha256,
                })
            })
        })
    })
})
movefile = (uploadFile, fileName) => {
    return new Promise(resolve => {
        uploadFile.mv(
            `${__basedir}/temp/${fileName}`,
            function (err) {
                if (err) {
                    console.log(err)
                }
                else {
                    resolve(true)
                }
            }
        )
    })
}
router.post('/verifier/verify', (req, res) => {
    let uploadFile = req.files.filepond
    const fileName = req.files.filepond.name

    movefile(uploadFile, fileName).then(data => {
        tempfilename = `${__basedir}/temp/${fileName}`
        fs.readFile(tempfilename, (err, readfile) => {
            if (err) {
                console.log("reader", err)
            }
            else {
                console.log(readfile)
                generatesha(readfile).then(hash => {
                    res.json({
                        File_Name: fileName,
                        SHA: hash,
                    })

                });
            }
        })

    })


    //const filedata=req.files.filepond.data
    //console.log("data",req.files.filepond.data)
    //console.log("hai",globalfile)

    //hash=generatesha(filedata); 
}
)


module.exports = router;