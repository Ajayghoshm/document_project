var router = require('express').Router();
const MyNetwork = require('../../block/mynetwork');
const validateRegisterInput = require("../../validation/register");
const User = require("../../models/User");
const bcrypt = require("bcryptjs");

// api/products
router.post('/student', function (req, res) {
console.log("student")
    // Form validation
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: "Email already exists" });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                id:req.body.id,
                typeid:"1"
            });
            const blockuser={
                firstName:req.body.name,
                id:req.body.id,
                lastName:req.body.email
            }
            // Hash password before saving in database
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then((user) => {
                            mynetwork = new MyNetwork()
                            mynetwork.createStudent(blockuser)
                            res.json(user)
                        }
                        )
                        .catch(err => console.log(err));
                });
            });
        }
    });
});

router.post('/university', function (req, res) {
    console.log("University")
    const { errors, isValid } = validateRegisterInput(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }
    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: "Email already exists" });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                typeid:"2",
                id:req.body.id
            });
            const blockUniversity={
                name:req.body.name,
                id:req.body.id,
            }

            // Hash password before saving in database
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then((user) => {
                            mynetwork = new MyNetwork()
                            mynetwork.createUniversity(blockUniversity)
                            res.json(user)
                        }
                        )
                        .catch(err => console.log(err));
                });
            });
        }
    });
});

router.post('/verifier', function (req, res) {
    console.log("Verifier")
    const { errors, isValid } = validateRegisterInput(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }
    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: "Email already exists" });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                typeid:"3",
                id:req.body.id
            });
            const blockVerifier={
                firstName:req.body.name,
                id:req.body.id,
                lastName:req.body.email
            }

            // Hash password before saving in database
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then((user) => {
                            mynetwork = new MyNetwork()
                            mynetwork.createVerifier(blockVerifier)
                            res.json(user)
                        }
                        )
                        .catch(err => console.log(err));
                });
            });
        }
    });
});

module.exports = router;