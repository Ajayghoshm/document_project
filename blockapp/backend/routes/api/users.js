const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");
var sha256File = require('sha256-file');
var multer = require('multer')
const crypto = require('crypto');
const MyNetwork = require('../../block/mynetwork');
var fs = require('fs');

var multiparty = require('connect-multiparty'),
  multipartyMiddleware = multiparty();

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images/uploads')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now() + '.card')
  }
});
var upload = multer({
  storage: storage, onFileUploadStart: function (file) {
    console.log(file.originalname + ' is starting ...')
  },
});


// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// Load User model
const User = require("../../models/User");

router.use('/block/register',require("./register"))
router.use('/block/login',require("./login"))

/*router.post("/register", (req, res) => {
  // Form validation

  const { errors, isValid } = validateRegisterInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      });

      // Hash password before saving in database
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
    }
  });
});*/

/*router.post("/login", (req, res) => {
  // Form validation
  console.log("login", req.body)

  const { errors, isValid } = validateLoginInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  User.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const payload = {
          id: user.id,
          name: user.name
        };

        // Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 1 year in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});*/

router.post("/upload", /*upload.single('ajayghosh_resume.pdf'),*/(req, res) => {
  console.log("uplasdoasda")
  /*console.log("accessed upload routesss")
  console.log(req.files.filepond.name)
  let uploadFile = req.files.filepond
  const fileName = req.files.filepond
  console.log(req.files.filepond)
  const file = req.files.filepond
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    res.send(error)
  }*/
  console.log(req)
  let uploadFile = req.files.filepond
  const fileName = req.files.filepond.name
  console.log(req.files.filepond)
  uploadFile.mv(
    `${__dirname}/uplad/${fileName}`,
    function (err) {
      if (err) {
        console.log(err)
        return res.status(500).send(err)
      }
    }
  )

  sha256File(`${__dirname}/uplad/${fileName}`, function (error, sum) {
    if (error) return console.log("error:", error);
    console.log("sum", sum)
    const sha256 = crypto.createHash('sha256').update(`${__dirname}/uplad/${fileName}`).digest('hex');
    console.log("sha25", sha256)
    // '345eec8796c03e90b9185e4ae3fc12c1e8ebafa540f7c7821fb5da7a54edc704'
    res.json({
      File_Name: `${__dirname}/uplad/${fileName}`,
      SHA: sha256,
    })
  })



});

/*router.post('/block/admin/login', multipartyMiddleware, function (req, res) {
  console.log("function");

  let university = {
    name: "KTU",
    id: '4000'
  }
  let studentDetails = {
    id: "5000",
    firstName: "ajay",
    lastName: "ghosh"
  }

  var VerifierDetails = {
    id: "6000",
    firstName: "TCS",
    lastName: "hr"
  }
  var transactionDetails = {
    documentid: "0000",
    universityid: university.id,
    studentid: studentDetails.id,
    studentname: studentDetails.firstName,
    documentname: "SSlC",
    sha: "sdasd",
    date: "asdasd"
  }
  var transferDetails = {
    studentid: studentDetails.id,
    asset: transactionDetails.documentid
  }

  var accessTransaction = {
    veriferid: VerifierDetails.id,
    asset: transactionDetails.documentid
  }

  mynetwork = new MyNetwork()
 //mynetwork.createUniversity(university)
  //mynetwork.createStudent(studentDetails)
  //mynetwork.createVerifier(VerifierDetails)
  // mynetwork.createAssetTransaction(transactionDetails)
  //mynetwork.transferOwnerTransaction(transferDetails)
  //mynetwork.addVerifierToAcess(accessTransaction);
  //mynetwork.adminView("admin@certificate-network")
  mynetwork.View({name:"admin@certificate-network"})
  //mynetwork.studentView()
  //mynetwork.verifierView()
  

  //mynetwork.connect();
  //mynetwork.createParticipant(1)


  //

  //
  //mynetwork.admin();
  /*var file=fs.readFileSync('admin@network.card',(err,data)=>{
    if(err)
    console.log("err",err)
    if(data)
    console.log("file",data)
  })
  MyNetwork.importCardToNetwork(file).then(function(idCardName) {
    if (!idCardName) {
        res.status(403).json({message: "Logging failed"});
    }
    res.json({message: "Logging Successful", accessToken: idCardName})    
}).catch(function (error) {
    res.status(403).json({message: "Login failed", error: error.toString()})    
})
})*/

module.exports = router;
