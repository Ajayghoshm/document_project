const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const AdminConnection = require('composer-admin').AdminConnection;
const idApi = require("composer-common").IdCard
const { BusinessNetworkDefinition, CertificateUtil, IdCard } = require('composer-common');
const cardStoreApi = require("composer-common").BusinessNetworkCardStore
const fs = require("fs")
let bizNetworkConnection


const cardStore = require('composer-common')
.NetworkCardStoreManager
.getCardStore({ type: 'composer-wallet-filesystem' });
let connection;

class MyNetwork {

    

    constructor() {
        this.bizNetworkConnection = new BusinessNetworkConnection();
       
    }

    /*async admin() {
        try {

            const connectionProfile = {
                name: 'embedded',
                'x-type': 'embedded'
            };

            const cardStore = require('composer-common')
                .NetworkCardStoreManager
                .getCardStore({ type: 'composer-wallet-inmemory' });

            const adminCardName = "admin"

            const credentials = CertificateUtil.generate({ commonName: 'admin' });

            const deployerMetadata = {
                version: 1,
                userName: 'PeerAdmin',
                roles: [ 'PeerAdmin', 'ChannelAdmin' ]
            };

            const deployerCard = new IdCard(deployerMetadata, connectionProfile);
            deployerCard.setCredentials(credentials);
            const deployerCardName = 'PeerAdmin';

            adminConnection = new AdminConnection({ cardStore: cardStore });

            await adminConnection.importCard(deployerCardName, deployerCard);
            await adminConnection.connect(deployerCardName);


            /*let bizNetworkConnection = new BusinessNetworkConnection();
            let connecion = await bizNetworkConnection.connect('admin@certificate-network')
            let factory = connecion.getFactory()
            const alice = factory.newResource('org.example.basic', 'Student', '55555');
            alice.firstName = 'Alice';
            alice.lastName = 'A';

            const participantRegistry = await bizNetworkConnection.getParticipantRegistry("org.example.basic" + '.Student');
            await participantRegistry.addAll([alice]);


            bizNetworkConnection.issueIdentity('org.example.basic.Student#55555', 'alice1')
            let adminConnection=new AdminConnection();
            let connection = await adminConnection.connect('admin@certificate-network')
            let metadata={
                "version": 1,
                "name":"ajay",
                "userName": "ajay",
                "description": "Alice's identity for basic-sample-network",
                "businessNetwork": "certificate-network",
                "enrollmentSecret": "123"
            }
            let connectionprofile =  { name: 'hlfv1',
                              type: 'hlfv1',
                              orderers: [ { url: 'grpc://localhost:7050' } ],
                              ca: { url: 'http://localhost:7054', name: 'ca.org1.example.com' },
                              peers: 
                               [ { requestURL: 'grpc://localhost:7051',
                                   eventURL: 'grpc://localhost:7053' } ],
                              channel: 'composerchannel',
                              mspID: 'Org1MSP',
                              timeout: 300 }
            let card= await new idApi(metadata,connectionprofile)
            card.toArchive("../newfolder")
            card.toDirectory('../newfolder',fs).then(function(){
            console.log(card.getUserName())
            })
            /*let name=await cardStoreApi.getAll();
            console.log(name)
        } catch (err) {
            console.log(err)
        }
    }*/

    async importCardForIdentity(cardName, identity) {

        const credentials = CertificateUtil.generate({ commonName: 'admin' });


        let adminConnection = await new AdminConnection({ cardStore: cardStore });

        const connectionProfile = {
            "name": "hlfv1",
            "x-type": "hlfv1",
            "x-commitTimeout": 300,
            "version": "1.0.0",
            "client": {
                "organization": "Org1",
                "connection": {
                    "timeout": {
                        "peer": {
                            "endorser": "300",
                            "eventHub": "300",
                            "eventReg": "300"
                        },
                        "orderer": "300"
                    }
                }
            },
            "channels": {
                "composerchannel": {
                    "orderers": [
                        "orderer.example.com"
                    ],
                    "peers": {
                        "peer0.org1.example.com": {}
                    }
                }
            },
            "organizations": {
                "Org1": {
                    "mspid": "Org1MSP",
                    "peers": [
                        "peer0.org1.example.com"
                    ],
                    "certificateAuthorities": [
                        "ca.org1.example.com"
                    ]
                }
            },
            "orderers": {
                "orderer.example.com": {
                    "url": "grpc://localhost:7050"
                }
            },
            "peers": {
                "peer0.org1.example.com": {
                    "url": "grpc://localhost:7051"
                }
            },
            "certificateAuthorities": {
                "ca.org1.example.com": {
                    "url": "http://localhost:7054",
                    "caName": "ca.org1.example.com"
                }
            }
        };

        const metadata = {
            userName: identity.userID,
            version: 1,
            enrollmentSecret: identity.userSecret,
            businessNetwork: "certificate-network"
        };
        const card = new IdCard(metadata, connectionProfile);
        await adminConnection.importCard(cardName, card);
    }

    async createUniversity(university) {
        try {
            let connecion = await this.bizNetworkConnection.connect('admin@certificate-network')
            let factory = connecion.getFactory()
            let UniversityAssets = await this.bizNetworkConnection.getParticipantRegistry('org.example.basic.University')
            let University = factory.newResource("org.example.basic", "University", university.id)
            University.Name = university.name
            await UniversityAssets.add(University)
            console.log("university added")
            let identity = await this.bizNetworkConnection.issueIdentity('org.example.basic.University#' + university.id, university.name)
            console.log("university id created")
            await this.importCardForIdentity(university.name, identity)
            console.log("added to wallet")
        } catch (err) {
            console.log(err)
        }
    }



    async createStudent(studentDetails) {
        try {
            let connecion = await this.bizNetworkConnection.connect('admin@certificate-network')
            let factory = connecion.getFactory()
            let StudentAssets = await this.bizNetworkConnection.getParticipantRegistry('org.example.basic.Student')
            let student = factory.newResource('org.example.basic', 'Student', studentDetails.id)
            student.firstName = studentDetails.firstName
            student.lastName = studentDetails.lastName
            await StudentAssets.add(student)
            console.log("student added")
            let identity=await this.bizNetworkConnection.issueIdentity('org.example.basic.Student#' + studentDetails.id, studentDetails.firstName)
            console.log("student id created")
            await this.importCardForIdentity(studentDetails.firstName, identity)
            console.log("added to wallet")
        }
        catch (err) {
            console.log(err)
        }
    }

    async createVerifier(VerifierDetails) {
        try {
            let connecion = await this.bizNetworkConnection.connect('admin@certificate-network')
            let factory = connecion.getFactory()
            let VerifierAssets = await this.bizNetworkConnection.getParticipantRegistry('org.example.basic.Verifier')
            let verifier = factory.newResource('org.example.basic', "Verifier", VerifierDetails.id)
            verifier.firstName = VerifierDetails.firstName
            verifier.lastName = VerifierDetails.lastName
            await VerifierAssets.add(verifier)
            console.log("verifer added")
           let identity=await this.bizNetworkConnection.issueIdentity('org.example.basic.Verifier#' + VerifierDetails.id, VerifierDetails.firstName)
            console.log("verifier id created")
            await this.importCardForIdentity(VerifierDetails.firstName, identity)
            console.log("added to wallet")
        } catch (err) {
            console.log(err)
        }
    }

    async createAssetTransaction(transactionDetails) {
        try {
            let connection = await this.bizNetworkConnection.connect('admin@certificate-network')
            let factory = connection.getFactory()
            let transaction = {
                "$class": "org.example.basic.createasset"
            }
            transaction.documentid = transactionDetails.documentid
            transaction.universityid = transactionDetails.universityid
            transaction.studentid = transactionDetails.studentid
            transaction.studentname = transactionDetails.studentname
            transaction.documentname = transactionDetails.documentname
            transaction.sha = transactionDetails.sha
            transaction.date = transactionDetails.date

            let serializer = connection.getSerializer()
            let resource = serializer.fromJSON(transaction)
            await this.bizNetworkConnection.submitTransaction(resource)
            console.log('Certificate created')
        }
        catch (err) {
            console.log(err)
        }
    }

    async transferOwnerTransaction(transferDetails) {
        try {
            let connection = await this.bizNetworkConnection.connect('admin@certificate-network')
            let factory = connection.getFactory()
            let transaction = {
                "$class": "org.example.basic.transfertostudent"
            }
            transaction.studentid = transferDetails.studentid
            transaction.asset = "resource:org.example.basic.Document#" + transferDetails.asset

            let serializer = connection.getSerializer()
            let resource = serializer.fromJSON(transaction)
            await this.bizNetworkConnection.submitTransaction(resource)
            console.log('Certificate Transfered')
        }
        catch (err) {
            console.log(err)
        }
    }

    async addVerifierToAcess(accessTransaction) {

        try {
            let connection = await this.bizNetworkConnection.connect('admin@certificate-network')
            let factory = connection.getFactory()
            let transaction = {
                "$class": "org.example.basic.addverifertoaccess"
            }
            transaction.veriferid = accessTransaction.verifierid
            transaction.asset = "resource:org.example.basic.Document#" + accessTransaction.asset
            let serializer = connection.getSerializer()
            let resource = serializer.fromJSON(transaction)
            await this.bizNetworkConnection.submitTransaction(resource)
            console.log('Certificate access provided')
        }
        catch (err) {
            console.log(err)
        }
    }

    async changeCard(cardName){
        try{
        await this.bizNetworkConnection.disconnect();
        let connection=this.bizNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
        await this.bizNetworkConnection.connect(cardName);
        console.log("connected using "+cardName)
        return connection
        }catch(err){
            console.log(err)
        }
       
    }

    async View(university){
        try{
    let connection = await this.changeCard(university)
    /*const studentresults = await connection.query("selectstudent");
    studentresults.forEach(element => {
       console.log(element.firstName)
       console.log(element.lastName)
       console.log(element.participantId)
    })*/
    const documentresults = await connection.query("selectdocument");
    documentresults.forEach(element => {
       console.log(element.owner.$identifier)
       console.log(element.documentname)
       console.log(element.assetId)
       console.log(element.studentname)
       console.log(element.sha)
       console.log(element.date)
       element.access.forEach(value=>{
           console.log(value.$identifier)
       })
    })
    return documentresults
}catch(err){
    console.log(err)
}
}
}

module.exports = MyNetwork;

